// pages/Home.tsx

import React from 'react';
import Link from 'next/link';

const Home: React.FC = () => {
  return (
    <div className="overflow-x-hidden pt-80">
      <h1>Welcome to the Home Page</h1>
      <p>This is the home page of the application.</p>
      <Link href="/ideas" legacyBehavior>
        <a>Go to Ideas Page</a>
      </Link>
    </div>
  );
};

export default Home;
