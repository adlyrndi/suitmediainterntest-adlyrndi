'use client'
import React, { useState, useEffect } from 'react';
import Card from '@/components/card';
import Dropdown from '@/components/dropdown';
import Pagination from '@/components/pagination';
import axios from 'axios';
import { NextPage } from 'next';

interface ImageData {
  id: number;
  mime: string;
  file_name: string;
  url: string;
}

interface CardData {
  id: number;
  attributes: {
    small_image: ImageData[];
    title: string;
    published_at: string;
  };
}

const Ideas: NextPage = () => {
  const [ideas, setIdeas] = useState<CardData[]>([]);
  const [selectedValueNumber, setSelectedValueNumber] = useState<string>('10'); 
  const [selectedValueSort, setSelectedValueSort] = useState<string>('Newest'); 
  const [currentPage, setCurrentPage] = useState<number>(1);

  useEffect(() => {
    fetchData();
  }, [selectedValueNumber, selectedValueSort, currentPage]);

  const getIdeasPage = async (page: number, sortby: string, pagecount: number) => {
    try {
      const res = await axios.get(
        `/api/ideas?page[number]=${page}&page[size]=${pagecount}&append[]=small_image&append[]=medium_image&sort=${sortby}`,
        { headers: { "Accept": "application/json" } }
      );
      return res.data;
    } catch (error) {
      console.error('Error fetching ideas:', error);
      return null;
    }
  };

  const fetchData = async () => {
    const sortParam = selectedValueSort === 'Newest' ? '-published_at' : 'published_at';
    const data = await getIdeasPage(currentPage, sortParam, parseInt(selectedValueNumber));

    if (data) {
      console.log(data); 
      setIdeas(data.data.map((item: any) => ({
        id: item.id,
        attributes: {
          small_image: item.small_image[0].url,
          title: item.title,
          published_at: item.published_at,
        }
      })));
    }
  };

  const handleDropdownNumber = (value: string) => {
    setSelectedValueNumber(value);
  };

  const handleDropdownSort = (value: string) => {
    setSelectedValueSort(value);
  };

  return (
    <div className='min-h-screen bg-white'>
     
      <div className="relative h-[500px] bg-gray-700">
        <div className="flex flex-col gap-4 justify-center items-center w-full h-full px-3 md:px-0">
          <h1 className="text-6xl font-bold text-white">Ideas</h1>
          <p className="text-white">Where all our great things begin</p>
        </div>
      </div>
      
      
      <div className='mt-12 ml-64 mr-64'>
        
        <div className="flex justify-between text-black mb-12 ">
          <p className='pt-2 text-sm'>
            Showing 1-{selectedValueNumber} of 100
          </p>
          <div className='flex space-x-10'>
            
            <div className="flex space-x-5">
              <p className="pt-2 text-sm">Show per pages</p>
              <Dropdown options={['10', '20', '50']} onChange={handleDropdownNumber} defaultValue={'10'} />
            </div>
            
           
            <div className="flex space-x-5">
              <p className="pt-2 text-sm">Sort by</p>
              <Dropdown options={['Newest', 'Oldest']} onChange={handleDropdownSort} defaultValue={'Newest'} />
            </div>
          </div>
        </div>
        
       
        <div className="pb-12 grid grid-cols-4 gap-8 justify-items-center">
          {ideas.map((idea) => (
           <Card
                key={idea.id}
                small_image={idea.attributes.small_image}
                title={idea.attributes.title}
                published_at={idea.attributes.published_at}
         />
         
          ))}
        </div>
        
        
         <div className='flex justify-center pb-24 pt-24'>
          <Pagination />
        </div>
      </div>
    </div>
  );
}

export default Ideas;
