

import React from 'react';

const Pagination: React.FC = () => {
  return (
    <div className="inline-flex rounded-xl">
      <ul className="flex items-center">
      <li className="px-2">
          
          <button
            aria-disabled="true"
            disabled
            className="w-9 h-9 flex items-center justify-center rounded-md border hover:border-cyan-500 hover:text-black"
          >
            <span>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"><path d="m13.707 4.707-1.414-1.414L3.586 12l8.707 8.707 1.414-1.414L6.414 12l7.293-7.293z"/><path d="m19.707 4.707-1.414-1.414L9.586 12l8.707 8.707 1.414-1.414L12.414 12l7.293-7.293z"/></svg>
            </span>
          </button>
        </li>
        <li className="px-2">
          
          <button
            aria-disabled="true"
            disabled
            className="w-9 h-9 flex items-center justify-center rounded-md border hover:border-cyan-500 hover:text-black"
          >
            <span>
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"><path d="M15.293 3.293 6.586 12l8.707 8.707 1.414-1.414L9.414 12l7.293-7.293-1.414-1.414z"/></svg>
            </span>
          </button>
        </li>
        <li className="px-2">
          <button className="w-9 h-9 rounded-md border hover:border-cyan-500 hover:text-black text-black">
            1
          </button>
        </li>
        <li className="px-2">
          <button className="w-9 h-9 rounded-md border hover:border-cyan-500 hover:text-black text-black">
            2
          </button>
        </li>
        <li className="px-2">
          <button className="w-9 h-9 rounded-md border hover:border-cyan-500 hover:text-black text-black">
            3
          </button>
        </li>
        <li className="px-2">
          <button className="w-9 h-9 rounded-md border hover:border-cyan-500 hover:text-black text-black">
            4
          </button>
        </li>
        <li className="px-2">
          <button className="w-9 h-9 rounded-md border hover:border-cyan-500 hover:text-black text-black">
            5
          </button>
        </li>
        <li className="px-2">
          <button
            aria-disabled="false"
            className="w-9 h-9 flex items-center justify-center rounded-md border hover:border-cyan-500 hover:text-black"
          >
            <span>
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"><path d="M7.293 4.707 14.586 12l-7.293 7.293 1.414 1.414L17.414 12 8.707 3.293 7.293 4.707z"/></svg>
            </span>
          </button>
        </li>
        <li className="px-2">
          
          <button
            aria-disabled="true"
            disabled
            className="w-9 h-9 flex items-center justify-center rounded-md border hover:border-cyan-500 hover:text-black"
          >
            <span>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"><path d="m11.707 3.293-1.414 1.414L17.586 12l-7.293 7.293 1.414 1.414L20.414 12l-8.707-8.707z"/><path d="M5.707 3.293 4.293 4.707 11.586 12l-7.293 7.293 1.414 1.414L14.414 12 5.707 3.293z"/></svg>
            </span>
          </button>
        </li>
      </ul>
    </div>
  );
};

export default Pagination;
