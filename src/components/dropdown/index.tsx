import React, { useState } from 'react';

interface DropdownProps {
  options: string[];
  onChange: (value: string) => void;
  defaultValue: string; 
}

const Dropdown: React.FC<DropdownProps> = ({ options, onChange, defaultValue }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedValue, setSelectedValue] = useState(defaultValue); 

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleOptionClick = (value: string) => {
    onChange(value);
    setSelectedValue(value);
    setIsOpen(false);
  };

  return (
    <div className="relative">
      <button
        id="Dropdown"
        onClick={toggleDropdown}
        className="text-black bg-white hover:bg-gray-300 border font-medium rounded-full text-sm px-7 py-2 text-center inline-flex items-center mr-1"
        type="button"
      >
        {selectedValue} 
        <svg
          className={`w-2.5 h-2.5 ml-1 ${isOpen ? 'transform rotate-180' : ''}`} 
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 10 6"
          
        >
          <path
            stroke="currentColor"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="m1 1 4 4 4-4"
          />
        </svg>
      </button>

      {/* Dropdown menu */}
      {isOpen && (
        <div
          id="dropdownHover"
          className="absolute z-10 bg-white divide-y divide-gray-100 rounded-lg shadow w-44 overflow-y-auto max-h-40"
        >
          <ul
            className="py-2 text-sm text-gray-700"
            aria-labelledby="dropdownHoverButton"
          >
            {options.map((option) => (
              <li key={option}>
                <a
                  className={`block px-4 py-2 hover:bg-gray-100 ${option === selectedValue ? 'bg-gray-100' : ''}`} 
                  onClick={() => handleOptionClick(option)}
                >
                  {option}
                </a>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};

export default Dropdown;
