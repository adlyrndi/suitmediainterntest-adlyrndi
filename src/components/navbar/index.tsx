;'use client';
import React, { useState, useEffect } from 'react';
import Image from 'next/image';
import { usePathname } from 'next/navigation';

const Navbar = () => {
    const [show, setShow] = useState(true);
    const [lastScrollY, setLastScrollY] = useState(0);
    const currentPath = usePathname();

    const isActive = (pathname: string) => currentPath === pathname;

    const controlNavbar = () => {
        if (typeof window !== 'undefined') {
            if (window.scrollY > lastScrollY) {
                setShow(false);
            } else {
                setShow(true);
            }
            setLastScrollY(window.scrollY);
        }
    };

    useEffect(() => {
        if (typeof window !== 'undefined') {
            window.addEventListener('scroll', controlNavbar);
            return () => {
                window.removeEventListener('scroll', controlNavbar);
            };
        }
    }, [lastScrollY]);

    return (
        <nav className={`fixed w-full transition-transform duration-300 ease-in-out ${show ? 'top-0' : '-top-full'} bg-[#f06d34] bg-opacity-90 z-50`}>
            <div className="container flex justify-between h-[11vh] items-center">
                <Image src="/final.png" alt="Logo" width={120} height={90} className="ml-[200px]" />
                <div className="links flex gap-10 text-white">
                    <a href={'/Work'} className={`text-white ${isActive("/Work") ? "border-b-4 border-white" : ""}`}>Work</a>
                    <a href={'/About'} className={`text-white ${isActive("/About") ? "border-b-4 border-white" : ""}`}>About</a>
                    <a href={'/Service'} className={`text-white ${isActive("/Service") ? "border-b-4 border-white" : ""}`}>Services</a>
                    <a href={'/ideas'} className={`text-white ${isActive("/ideas") ? "border-b-4 border-white" : ""}`}>Ideas</a>
                    <a href={'/careers'} className={`text-white ${isActive("/careers") ? "border-b-4 border-white" : ""}`}>Careers</a>
                    <a href={'/contact'} className={`text-white ${isActive("/contact") ? "border-b-4 border-white" : ""}`}>Contact</a>
                </div>
            </div>
        </nav>
    );
};

export default Navbar;
