import React from 'react';

interface ImageData {
  id: number;
  mime: string;
  file_name: string;
  url: string;
}

interface CardProps {
  small_image: ImageData[]; 
  title: string;
  published_at: string;
}

const Card: React.FC<CardProps> = ({ small_image, title, published_at }) => {
  
  const imageUrl = small_image.length > 0 ? small_image[0].url : '';

  return (
    <div className="max-w-sm rounded-lg overflow-hidden shadow-xl bg-white">
      <a href="#">
        {imageUrl && ( 
          <img className="rounded-t-lg" src={imageUrl} alt={title} />
        )}
      </a>
      <div className="p-5">
        <p className="mb-3 text-gray-500">
          {published_at}
        </p>
        <h5 className="mb-2 text-2xl font-bold tracking-tight text-black">
          {title}
        </h5>
      </div>
    </div>
  );
};

export default Card;
